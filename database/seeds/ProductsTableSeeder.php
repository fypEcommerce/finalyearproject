<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name'=> 'Laptop 1',
            'slug'=> 'laptop-1',
            'description'=>'omg1',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 2',
            'slug'=> 'laptop-2',
            'description'=>'omg2',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 3',
            'slug'=> 'laptop-3',
            'description'=>'omg3',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 4',
            'slug'=> 'laptop-4',
            'description'=>'omg4',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 5',
            'slug'=> 'laptop-5',
            'description'=>'omg5',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 6',
            'slug'=> 'laptop-6',
            'description'=>'omg6',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 7',
            'slug'=> 'laptop-7',
            'description'=>'omg7',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 8',
            'slug'=> 'laptop-8',
            'description'=>'omg8',
            'price'=> 249999,
        ]);
        Product::create([
            'name'=> 'Laptop 9',
            'slug'=> 'laptop-9',
            'description'=>'omg9',
            'price'=> 249999,
        ]);
    }
}
