<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


route::get('/','LandingPageController@index');
route::get('/shop','ShopPageController@index');
route::get('/shop/{product}','ShopPageController@showProduct');
route::get('/cart','CartPageController@index')->middleware('auth');


route::post('/cart/add','CartPageController@addItem')->middleware('auth');
route::post('/cart/update','CartPageController@updateQuantity');
route::get('/cart/delete/{productID}','CartPageController@deleteItem');

route::get('/checkout','CheckoutPageController@getCheckout');
route::post('/stripe-checkout','CheckoutPageController@stripeCheckout');
route::post('/paypal-checkout','CheckoutPageController@paypalCheckout');

route::get('/thankyou','OrderConfirmationPageController@showConfirmationPage');

route::get('/profile','CustomerController@showProfile')->middleware('auth');
route::post('/profile','CustomerController@updateAccount');
route::get('/order','CustomerController@viewOrderSummary')->middleware('auth');

route::get('/order/{order}','CustomerController@viewOrderDetail')->middleware('auth');

route::post('/coupon','CheckoutPageController@checkCoupon');
route::get('/coupon/remove','CheckoutPageController@removeCoupon');

route::get('/addressbook','CustomerController@showAddressBook');
route::post('/addressbook/save','CustomerController@saveNewAddress');
route::post('/addressbook/select','CustomerController@ajaxRetrieveAddress');
route::post('/addressbook/edit','CustomerController@editAddress');
route::get('/addressbook/default/{address}','CustomerController@setDefaultAddress');
route::get('/addressbook/delete/{address}','CustomerController@deleteAddress');

route::view('/product','product');
Auth::routes();

