@extends('layout')
@section('title','Order')
@section('content')
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
                <!-- row -->
                <div class="row">
                        <div class="col-md-12">
                                <ul class="breadcrumb-tree">
                                        <li><a href="/">Home</a></li>
                                        <li class="active">My Orders</li>
                                </ul>
                        </div>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
    </div>
      <!-- /section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row mobile">
                <!-- ASIDE -->
                <div id="aside" class="col-md-3 reverseDiv" style="border-right: 1px solid #eae9e9;">
                    <!-- aside Widget -->
                    <div class="aside">
                        <div>
                            <h3 class="aside-title" style="margin-bottom:36px;font-size:26px;" >My Account</h3>
                            <div>
                                <a href="/profile"><p> Account Information</p></a>
                            </div>
                            <div>
                                <a href="/addressbook"><p> Address Book</p></a>
                            </div>
                            <div>
                                <p class="active"> My Orders</p>
                            </div>
                            <div>
                                <a href=""><p> My Wish List</p></a>
                            </div>
                        </div>
                    </div>
                    <!-- /aside Widget -->
                </div>
            
            
                <div class="col-md-9">
                    <div style="margin-top:0 !important;margin-bottom:16px;" class="section-title">
                        <h3 class="title" style="text-transform: uppercase;font-size:16px;">My Orders</h3>
                    </div>

                    @if (count($orders) > 0)
                        @foreach($orders as $order)
                        <div class="order-container">
                            <div class="order-header">
                                <div class="order-header-items">
                                    <div>
                                        <div style="text-transform: uppercase;font-weight:bold;margin-right:15px;">Order Placed</div>
                                        <div>{{ date('M j, Y',strtotime($order->created_at))}}</div>
                                    </div>
                                    <div>
                                        <div style="text-transform: uppercase;font-weight:bold;margin-right:15px;" >Order ID</div>
                                        <div>{{$order->id}}</div>
                                    </div><div>
                                        <div style="text-transform: uppercase;font-weight:bold;margin-right:15px;" >Total</div>
                                        <div>${{$order->total_amount}}</div>
                                    </div>
                                </div>
                                <div>
                                    <div class="order-header-items">
                                        <div><a style="font-weight:normal;" href="/order/{{$order->id}}">Order Details</a></div>
                                    </div>
                                </div>
                            </div>
                            @foreach($order->order_products as $order_product)
                            <div class="order-products">
                                <div class="order-product-item">
                                    <div><img weight="80" height="80" src="{{asset('img/productsImage/'.$order_product->slug.'.jpg')}}" alt=""></div>
                                    <div>
                                        <div>
                                            <a class="product-name" style="font-weight:bold;font-size:15px;" href="/shop/{{$order_product->slug}}">{{$order_product->name}}</a>
                                        </div>
                                        <div>RM {{$order_product->price_per_product}}</div>
                                        <div>Quantity: {{$order_product->quantity}}</div>
                                    </div>
                                </div>   
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                    @else
                        <div class="col-md-12" style="text-align:center;padding-bottom:10%;">
                            <p style="text-align:center;margin-top:5%;font-size:20px;color: #555;">There are no orders placed yet.</p>
                            <a class="btn btn-default" style="margin-top:2%;padding: 10px 8px;background-color:#D10024;color:white;" href="/">Continue Shopping</a>    
                        </div>
                    @endif
                </div>    
                <!-- /STORE -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@endsection