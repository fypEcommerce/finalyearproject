@extends('layout')
@section('title','Checkout')
@section('content')
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
            @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(count($errors) > 0)
                <div style="margin-top:2%;" class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb-tree">
                            <li><a href="/">Home</a></li>
                            <li><a href="/cart">Shopping Cart</a></li>
                            <li class="active">Checkout</li>
                    </ul>
            </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>

    <div class="section">
        <div class="container">
            <div class=row>
                {{-- Order Detail --}}
                <div class="col-md-12 order-details" >
                    <div class="section-title text-center">
                        <h3 class="title">Your Order</h3>
                    </div>
                    <div class="order-summary">
                        <div >
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width:40%;">Product Ordered </th>
                                        <th scope="col" style="width:20%;">Unit Price</th>
                                        <th scope="col" style="width:20%" >Qty</th>
                                        <th scope="col">Item Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cartProducts as $cartProduct)
                                        <tr>
                                            <td><img style="height:50px; width:100px;" src="{{asset('img/productsImage/'.$cartProduct->slug.'.jpg')}}" alt=""><label style="margin-left:5%;text-transform:uppercase;">{{$cartProduct->name}}</label></td>
                                            <td>RM {{$cartProduct->price_per_product}}</td>
                                            <td>{{$cartProduct->product_quantity}}</td>
                                            <td>RM {{$cartProduct->total_product_price}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="border:none" colspan="3">Merchandise Subtotal</td>
                                        <td style="border:none"><strong>RM {{$totalPrice}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="border:none" colspan="3">Shipping Total</td>
                                        <td style="border:none"><strong>FREE</strong></td>
                                    </tr>  
                                    <tr>
                                        @if(session()->has('coupon'))
                                            <td style="border:none" colspan="3">Discount ({{session()->get('coupon')['name']}}) <a style="color:red;" href="/coupon/remove">Remove</a></td>
                                            <td style="border:none"><strong>-RM {{session()->get('coupon')['discount']}}</strong></td>
                                        @endif
                                    </tr>
                                    <tr>
                                            <td style="border:none" colspan="3"><strong>TOTAL PAYMENT</strong></td>
                                            <td style="border:none"><strong style="font-size: 24px;
                                                color: #D10024;">RM {{$Newtotalprice}}</strong></td>
                                        </tr> 
                                </tfoot>   
                            </table>   
                            @if(!session()->has('coupon'))
                                <a href="" class="have-code">Have a Code?</a>
                                <div >
                                    <form action="/coupon" method="post">
                                        @csrf
                                        <input style="padding:16px;" type="text" name="coupon_code" id="coupon_code">
                                        <button  type="submit" class="buttonDiscount">Apply</button>
                                    </form>
                                </div>
                            @endif
                        </div>
                     
                        
                    </div>
                   
                </div>
                <!-- /Order Details -->

                
                <div class="col-md-12">
                    <form action="/stripe-checkout" method="post" id="payment-form">
                        @csrf
                        <div style="margin-top:5%;" class="section-title">
                                <h3 class="title">Delivery Address</h3>
                            </div>
                        @if($defaultShipping)
                            <div class="form-group">
                                <div><strong>{{$defaultShipping->name}} ({{$defaultShipping->phone}}) </strong> <span style="margin-bottom:2%;" class="shippingdefault">default</span></div> 
                                <div style="margin-bottom:2%;">{{$defaultShipping->address}} , {{$defaultShipping->city}}, {{$defaultShipping->postcode}}, {{$defaultShipping->state}}</div>
                                <div><a class="btn btn-default" href="/addressbook">Manage Addresses</a></div>
                                <input type="hidden" id="billing-address" value="{{$defaultShipping->address}}">
                                <input type="hidden" id="billing-city" value="{{$defaultShipping->city}}">
                                <input type="hidden" id="billing-state" value="{{$defaultShipping->state}}">
                                <input type="hidden" id="billing-zip" value="{{$defaultShipping->postcode}}">
                            </div>
                        @endif
                        <div style="margin-top:5%;" class="section-title">
                            <h3 class="title">Payment Method</h3>
                        </div>
                        <div class="form-group">
                            <p>Name on Card</p>
                            <input class="input" type="text" id="card-name" placeholder="Name on Card">
                        </div>
                    
                        <div class="form-group">
                            
                            <p> Credit or debit card</p>
                            
                            <div id="card-element">
                            <!-- A Stripe Element will be inserted here. -->
                            </div>
                        
                            <!-- Used to display form errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>
                    
                        <input type="hidden" name="Newtotalprice" value="{{$Newtotalprice}}">
                        <button type="submit" id="completeOrderBtn" class="completeOrderBtn">Complete Order</button> 
                    </form>

                    <div style="margin-top:5%;" class="section-title">
                        <h3 class="title">Pay with Paypal</h3>
                    </div>
                    <form action="/paypal-checkout" method="post" id="paypal-payment">
                        @csrf
                        <section>
                            <div class="bt-drop-in-wrapper">
                                <div id="bt-dropin"></div>
                            </div>
                        </section>
                        <input type="hidden" name="Newtotalprice" value="{{$Newtotalprice}}">
                        <input id="nonce" name="payment_method_nonce" type="hidden" />
                        <button class="completeOrderBtn" type="submit"><span>Pay with PayPal</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

   
     <!-- Modal -->
     <form action="/addressbook/save" method="post">
        @csrf
        <div id="addNewAddressModalCheckout" class="modal fade" role="dialog">
            <div class="modal-dialog modalCenter">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add A New Address</h4>
                        <label style="font-size: 12px;">Please add a delivery address in order to place order</label>
                    </div>
                    <div class="modal-body">
                        <div style="margin-top:2%;">
                            <input type="text" class="form-control customInput" name="name"  placeholder="Name">
                        </div>
                        <div style="margin-top:2%;">
                            <input type="text" class="form-control customInput" name="phonenumber" placeholder="Phone Number">
                        </div>
                        <div style="margin-top:2%;">
                            <input type="text" class="form-control customInput" name="address" placeholder="Address">
                        </div>
                        <div style="margin-top:2%;">
                            <input type="text" class="form-control customInput" name="state" placeholder="State">
                        </div>
                        <div style="margin-top:2%;">
                            <input type="text" class="form-control customInput" name="city" placeholder="City">
                        </div>
                        <div style="margin-top:2%;">
                            <input type="text" class="form-control customInput" name="postalcode" placeholder="Postal Code">
                        </div>     
                        <label style="opacity: .35; cursor: no-drop;" class="checkboxContainer">Set as default address
                            <input type="hidden" name="default" value="T" id ="default">
                            <input type="checkbox" name="default" value="T" checked disabled>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" href="/cart">Close</a>    
                        <button type="submit" class="btn" style="background: #D10024;color:#fff;border:none;padding: 6px 12px;">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('paymentgateway-js')
     @if(!$defaultShipping)
        <script>
            $(function() {
                $('#addNewAddressModalCheckout').modal('show');
                
            });

            $('#addNewAddressModalCheckout').on('hidden.bs.modal', function (e) {
                window.location.href = "/cart";
            });
            
        </script>
    @endif
    <script>
        var paypalToken = "{{$paypalToken}}";
    </script>
    <script src="{{asset('frontend/js/checkout.js')}}"></script>
    
@endsection