@extends('layout')
@section('title','Order')
@section('content')
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
                <!-- row -->
                <div class="row">
                        <div class="col-md-12">
                                <ul class="breadcrumb-tree">
                                        <li><a href="/">Home</a></li>
                                        <li class="active">My Orders</li>
                                </ul>
                        </div>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
    </div>
      <!-- /section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row mobile">
                <!-- ASIDE -->
                <div id="aside" class="col-md-3 reverseDiv" style="border-right: 1px solid #eae9e9;">
                    <!-- aside Widget -->
                    <div class="aside">
                        <div>
                            <h3 class="aside-title" style="margin-bottom:36px;font-size:26px;" >My Account</h3>
                            <div>
                                <a href="/profile"><p> Account Information</p></a>
                            </div>
                            <div>
                                <a href="/addressbook"><p> Address Book</p></a>
                            </div>
                            <div>
                                <p class="active"> My Orders</p>
                            </div>
                            <div>
                                <a href=""><p> My Wish List</p></a>
                            </div>
                        </div>
                    </div>
                    <!-- /aside Widget -->
                </div>
            
            
                <div class="col-md-9">
                    <div style="margin-top:0 !important;margin-bottom:16px;" class="section-title">
                        <h3 class="title" style="text-transform: uppercase;font-size:16px;">Order Details</h3>
                    </div>
                    <p>Ordered on {{ date('M j, Y',strtotime($order->created_at))}} <span style="color:#888888;">|</span> Order ID : {{$order->id}}</p>
                 
                       
                  
                    <div style="border:1px solid #CCCCCC;">
                        <div class="order-header">
                            Shipping Address
                        </div>
                        <div style="padding:15px;" >
                            <p><strong style="text-transform: uppercase;">{{$shipping_address->name}} ({{$shipping_address->phone}})</strong></p>
                            <p><small>{{$shipping_address->address}} , {{$shipping_address->city}}, {{$shipping_address->postcode}}, {{$shipping_address->state}}</small></p>
                        </div>
                    </div>
                   
                
                
                 
                    <div style="border:1px solid #CCCCCC;margin-top:2%;">
                        <div class="order-header">
                            Order Summary
                        </div>
                        <div style="padding:15px;height:100px;" >
                            <div style="float:left;">
                                <p><strong>Total Items : </strong><small>{{$order->total_quantity}}</small></p>
                                <p><strong>Total Price  : </strong><small>RM {{$order->total_amount}}</small></p>
                            </div>
                            <div>
                                <p><strong>Payment Method : </strong><small>{{$order->payment_gateway}}</small></p>
                            </div>
                        </div>
                    </div>
                 
                    
                    <div style="border:1px solid #CCCCCC;margin-top:2%;margin-bottom:2%;">
                        <div class="order-header">
                            Order Items
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    @foreach($order_products as $order_product)
                                        <tr>
                                            <td><a href="/shop/{{$order_product->slug}}"><img src="{{asset('img/productsImage/'.$order_product->slug.'.jpg')}}" weight="50" height="50" alt="cola"></a></td>
                                            <td><a href="/shop/{{$order_product->slug}}">{{$order_product->name}}</a></td>
                                            <td>RM {{$order_product->price_per_product}}</td>
                                            <td>x{{$order_product->quantity}}</td>
                                            <td>RM {{$order_product->price}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>   
                            </table>         
                        </div>
                    </div>
                    <a class="primary-btn cta-btn" href="/order">Back</a>
                </div>

                   
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@endsection