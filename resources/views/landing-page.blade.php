{{-- <!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ECSDM</title>

		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/slick-theme.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/nouislider.min.css')}}"/>
		<link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/style.css')}}"/>
    </head>
    <body>
		@include('partials.nav')


		<div id="new-arrival" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="hot-deal">
							
							<h2 class="text-uppercase" style="color:#FFFFFF;">Check out more product !</h2>
							<a class="primary-btn cta-btn" href="/shop">Shop now</a>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /HOT DEAL SECTION -->



		<!-- SECTION -->
		<div class="section" style="margin-bottom:5%;" >
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Top Selling Products</h3>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab2" class="tab-pane fade in active">
									<div class="products-slick" data-nav="#slick-nav-2">
									
										@foreach($products as $product)
											
												<div class="product">
													<a href="/shop/{{$product->slug}}">
														<div class="product-img">
															<img src="{{asset('img/productsImage/'.$product->slug.'.jpg')}}" alt="">	
														</div>
													</a>
													<div class="product-body">
														<h3 class="product-name"><a href="/shop/{{$product->slug}}">{{$product->name}}</a></h3>
														<h4 class="product-price">RM {{$product->price}}</h4>	
													</div>
												</div>
											
										@endforeach	
									</div>
									<div id="slick-nav-2" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- /Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>

		@include('partials.footer')

		<script src="{{asset('frontend/js/jquery.min.js')}}"></script>
		<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('frontend/js/slick.min.js')}}"></script>
		<script src="{{asset('frontend/js/nouislider.min.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.zoom.min.js')}}"></script>
		<script src="{{asset('frontend/js/main.js')}}"></script>
    </body>
</html> --}}

@extends('layout')
@section('title','Home')
@section('content')
	<div id="new-arrival" class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<div class="hot-deal">
						
						<h2 class="text-uppercase" style="color:#FFFFFF;">Check out more product !</h2>
						<a class="primary-btn cta-btn" href="/shop">Shop now</a>
					</div>
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /HOT DEAL SECTION -->



	<!-- SECTION -->
	<div class="section" style="margin-bottom:5%;" >
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">

				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h3 class="title">Top Selling Products </h3>
					</div>
				</div>
				<!-- /section title -->
				
				<!-- Products tab & slick -->
				<div class="col-md-12">
					<div class="row">
						<div class="products-tabs">
							<!-- tab -->
							<div id="tab2" class="tab-pane fade in active">
								<div class="products-slick" data-nav="#slick-nav-2">
								
									@foreach($products as $product)
										
											<div class="product">
												<a href="/shop/{{$product->slug}}">
													<div class="product-img">
														<img src="{{asset('img/productsImage/'.$product->slug.'.jpg')}}" alt="">	
													</div>
												</a>
												<div class="product-body">
													<h3 class="product-name"><a href="/shop/{{$product->slug}}">{{$product->name}}</a></h3>
													<h4 class="product-price">RM {{$product->price_per_product}}</h4>	
												</div>
											</div>
										
									@endforeach	
								</div>
								<div id="slick-nav-2" class="products-slick-nav"></div>
							</div>
							<!-- /tab -->
						</div>
					</div>
				</div>
				<!-- /Products tab & slick -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
@endsection