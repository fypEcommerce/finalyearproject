<div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Related Product</h3>
                    </div>
                </div>

                @foreach($relatedProducts as $relatedProduct)
                    <a href="/shop/{{$relatedProduct->slug}}">
                        <div class="col-md-3 col-xs-6">
                            <div class="product">
                                <div class="product-img">
                                    <img src="{{asset('img/productsImage/'.$relatedProduct->slug.'.jpg')}}" alt="">
                                </div>
                                <div class="product-body">
                                    <h3 class="product-name"><a href="/shop/{{$relatedProduct->slug}}">{{$relatedProduct->name}}</a></h3>
                                    <h4 class="product-price">RM {{$relatedProduct->price_per_product}}<h4>
                                </div>
                            </div>
                         </div>
                    </a>
                @endforeach
               
               

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /Section -->