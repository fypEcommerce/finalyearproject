<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-right">
                @guest
                    <li><a href="/login"><i class="fa fa-user-o"></i> Login</a></li>
                    <li style="color:#FFFFFF;">New Customer? <a style="text-decoration:underline;" href="/register">Start Here.</a></li>
                @else
                    <li style="color:#FFFFFF">Hello {{Auth::user()->name}}</li>
                    <li><a href="/order">My Orders</a></li>
                    <li><a href="/profile">My Account</a></li>
                    <li><a style="text-decoration:underline;" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                @endguest
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->


    <!-- MAIN HEADER -->
    <div id="header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- LOGO -->
                <div class="col-md-3">
                    <div class="header-logo">
                        <a href="/" class="logo">
                            <img src="{{asset('frontend/img/logooo.png')}}" alt="ECSDM" width="100" height="70">
                        </a>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- SEARCH BAR -->
                <div class="col-md-6">
                    <div class="header-search">
                        <form>
                            <select class="input-select" style="outline:none;">
                                <option value="0">All Categories</option>
                                <option value="1">Category 01</option>
                                <option value="1">Category 02</option>
                            </select>
                            <input class="input" placeholder="Search here">
                            <button class="search-btn">Search</button>
                        </form>
                    </div>
                </div>
                <!-- /SEARCH BAR -->

                <!-- ACCOUNT -->
                <div class="col-md-3 clearfix">
                    <div class="header-ctn">
                            
                        <!-- Wishlist -->
                        <div>
                            <a href="#">
                                <i class="fa fa-heart-o"></i>
                                <span>Your Wishlist</span>
                                <div class="qty">2</div>
                            </a>
                        </div>
                        <!-- /Wishlist -->

                        <!-- Cart -->
                        <div>
                            <a href="/cart">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Your Cart</span>
                                @if (Auth::check() && $cart_count!=0) 
                                    <div class="qty">{{$cart_count}}</div>
                                @endif
                            </a>
                        </div>
                        <!-- /Cart -->
                    </div>
                </div>
                <!-- /ACCOUNT -->
            </div>
                <!-- row -->
        </div>
        <!-- container -->
    </div>
        <!-- /MAIN HEADER -->
</header>