<!DOCTYPE html>
<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ECSDM | @yield('title','')</title>

		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/slick-theme.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/nouislider.min.css')}}"/>
		<link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
		<link type="text/css" rel="stylesheet" href="{{asset('frontend/css/style.css')}}"/>
		<script src="https://js.stripe.com/v3/"></script>
		<script src="https://js.braintreegateway.com/web/dropin/1.14.1/js/dropin.min.js"></script>
		
    </head>
    <body>
		@include('partials.header')
		@yield('content')
		@include('partials.footer')
		
		<script src="{{asset('frontend/js/jquery.min.js')}}"></script>
		<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('frontend/js/slick.min.js')}}"></script>
		<script src="{{asset('frontend/js/nouislider.min.js')}}"></script>
		<script src="{{asset('frontend/js/jquery.zoom.min.js')}}"></script>
		<script src="{{asset('frontend/js/main.js')}}"></script>
		@yield('paymentgateway-js')
    </body>
</html>

<script>
	function showChangePasswordForm(){
		if(document.getElementById('change-password').checked)
			$('.hidefield').prop('required',true);
		else	
			$('.hidefield').prop('required',false);	
	}

	$(document).ready(function(){
		setTimeout(function() {
			$('#MessageBox').fadeOut('slow');	
		}, 2000); 

		// $("#productquantity").bind('change keyup', function () {
		// 	var newqty = $('#productquantity').val(); //get the new quantity
		// 	var productID = $(this).attr("data-id");    //get the product id through data-id attribute
		// 	alert(productID);

		// 	$.post('/cart/update',{NewQty:newqty,ProductID:productID}, function(response) {
		// 		//handle response
		// 	})
		// });
	});

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});



	var typingTimer;     // To slow down the javascript call          
	$("input[type=number]").on("keyup change", function(){
		clearTimeout(typingTimer);
		var $this = this;
		typingTimer = setTimeout(function(){
			var newqty = $($this).val(); //get the new quantity
			var productID = $($this).attr("data-id");    //get the product id through data-id attribute
			// alert(newqty);
			// alert(productID);
			$.ajax({
				type:'POST',
				url:'/cart/update',
				data: {NewQty: newqty,ProductID:productID},
				success:function(data){
					location.reload();
				
				},
				error: function(){
					alert("error");
				},
			});
		}, 600);
		
	});

	$('.editAddressBtn').on('click', function () {
		var addressid = $(this).attr("id");
		$.ajax({
			type:'POST',
			url:'/addressbook/select',
			data: {addressid: addressid},
			success:function(data){
				var parsed_data = JSON.parse(data);
				$('#newname').val(parsed_data.name);
				$('#newphone').val(parsed_data.phone);
				$('#newaddress').val(parsed_data.address);
				$('#newstate').val(parsed_data.state);
				$('#newcity').val(parsed_data.city);
				$('#newpostalcode').val(parsed_data.postcode);
				$('#addressid').val(parsed_data.addressid);
			},
		});

	})


</script>