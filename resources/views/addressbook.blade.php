@extends('layout')
@section('title','My Account')
@section('content')
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
                <!-- row -->
                <div class="row">
                        <div class="col-md-12">
                                <ul class="breadcrumb-tree">
                                        <li><a href="/">Home</a></li>
                                        <li class="active">Address Book</li>
                                </ul>
                        </div>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
    </div>
      <!-- /section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row mobile">
                <!-- ASIDE -->
                <div id="aside" class="col-md-3 reverseDiv" style="border-right: 1px solid #eae9e9;">
                    <!-- aside Widget -->
                    <div class="aside">
                        <div>
                            <h3 class="aside-title" style="margin-bottom:36px;font-size:26px;" >My Account</h3>
                            <div>
                                <a href="/profile"><p> Account Information</p></a>
                            </div>
                            <div>
                                <p class="active"> Address Book</p>
                            </div>
                            <div>
                                <a href="/order"><p> My Orders</p></a>
                            </div>
                            <div>
                                <a href=""><p> My Wish List</p></a>
                            </div>
                        </div>
                    </div>
                    <!-- /aside Widget -->
                </div>
                <!-- /ASIDE -->

                <!-- STORE -->
             
                @csrf
                <div class="col-md-9">
                    @if (session()->has('success_message'))
                        <div class="alert alert-success">
                            {{ session()->get('success_message') }}
                        </div>
                    @endif
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div style="margin-top:0 !important;margin-bottom:16px;" class="section-title">
                        <div style="float:left;"><h3 class="title" style="text-transform: uppercase;font-size:16px;padding:15px">My Addresses</h3></div>
                        <div><button style="float:right;" class="addNewAddress" data-toggle="modal" data-target="#addNewAddressModal"><i style="padding-right:10px;" class="fa fa-plus"></i>Add New Address</button></div>
                    </div>     
                </div>

               
                <!-- Modal -->
                <form action="/addressbook/save" method="post">
                    @csrf
                    <div id="addNewAddressModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modalCenter">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Add A New Address</h4>
                                </div>
                                <div class="modal-body">
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="name"  placeholder="Name">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="phonenumber" placeholder="Phone Number">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="address" placeholder="Address">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="state" placeholder="State">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="city" placeholder="City">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="postalcode" placeholder="Postal Code">
                                    </div>
                                    @if(!$shipping_addresses->isEmpty())
                                    <label class="checkboxContainer">Set as default address
                                        <input type="checkbox" name="default" value="T">
                                    @else
                                    <label style="opacity: .35; cursor: no-drop;" class="checkboxContainer">Set as default address
                                        <input type="hidden" name="default" value="T" id ="default">
                                        <input type="checkbox" name="default" value="T" checked disabled>
                                    @endif
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn" style="background: #D10024;color:#fff;border:none;padding: 6px 12px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <form action="/addressbook/edit" method="post">
                    @csrf
                    <div id="editAddressModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modalCenter">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Edit Address</h4>
                                </div>
                                <div class="modal-body">
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="name" id="newname" placeholder="Name">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="phonenumber" id="newphone" placeholder="Phone Number">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="address" id="newaddress" placeholder="Address">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="state" id="newstate" placeholder="State">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="city" id="newcity" placeholder="City">
                                    </div>
                                    <div style="margin-top:2%;">
                                        <input type="text" class="form-control customInput" name="postalcode" id="newpostalcode" placeholder="Postal Code">
                                    </div>
                                </div>
                                <input type="hidden" name="addressid" id ="addressid">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn" style="background: #D10024;color:#fff;border:none;padding: 6px 12px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                @if(count($shipping_addresses) > 0)
                    <div class="col-md-9" >
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th scope="col" style="width:20%;">Name</th>
                                <th scope="col" style="width:25%;">Address</th>
                                <th scope="col" style="width:25%;" >Postcode</th>
                                <th scope="col" style="width:5%;">Tel</th>
                                <th scope="col" style="width:30%;"></th>
                                </tr>
                            </thead>
                            <tbody style="font-size:13px;">
                                @foreach($shipping_addresses as $shipping_address)
                                    <tr>
                                        <td>
                                            <div>{{$shipping_address->name}}</div>
                                            @if($shipping_address->defaultshipping == "T")
                                                <div class="shippingdefault">default</div>
                                            @else
                                                <a class="btn btn-default" href="/addressbook/default/{{$shipping_address->id}}">Set as default</a>    
                                            @endif
                                        </td>
                                        <td>{{$shipping_address->address}}</td>
                                        <td>{{$shipping_address->state}} - {{$shipping_address->city}} - {{$shipping_address->postcode}}</td>
                                        <td>{{$shipping_address->phone}}</td>
                                        <td>
                                                <a style="padding:0px 5px;" data-toggle="modal" id={{$shipping_address->id}} data-target="#editAddressModal" class="editAddressBtn btn btn-default" href=""><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-default" style="padding:0px 5px;" href="/addressbook/delete/{{$shipping_address->id}}"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="col-md-9">
                        <p style="text-align:center;margin-top:5%;font-size:20px;color: #555;">You don't have addresses yet.</p>
                    </div>
                @endif
                <!-- /STORE -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@endsection