@extends('layout')
@section('title','My Account')
@section('content')
    <div id="breadcrumb" class="section">
        <!-- container -->
        <div class="container">
                <!-- row -->
                <div class="row">
                        <div class="col-md-12">
                                <ul class="breadcrumb-tree">
                                        <li><a href="/">Home</a></li>
                                        <li class="active">My Account</li>
                                </ul>
                        </div>
                </div>
                <!-- /row -->
        </div>
        <!-- /container -->
    </div>
      <!-- /section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row mobile">
                <!-- ASIDE -->
                <div id="aside" class="col-md-3 reverseDiv" style="border-right: 1px solid #eae9e9;">
                    <!-- aside Widget -->
                    <div class="aside">
                        <div>
                            <h3 class="aside-title" style="margin-bottom:36px;font-size:26px;" >My Account</h3>
                            <div>
                               <p class="active"> Account Information</p>
                            </div>
                            <div>
                                <a href="/addressbook"><p> Address Book</p></a>
                            </div>
                            <div>
                                <a href="/order"><p> My Orders</p></a>
                            </div>
                            <div>
                                <a href=""><p> My Wish List</p></a>
                            </div>
                        </div>
                    </div>
                    <!-- /aside Widget -->
                </div>
                <!-- /ASIDE -->

                <!-- STORE -->
                <form method="post" action="/profile">
                    @csrf
                    <div class="col-md-9">
                        @if (session()->has('success_message'))
                            <div class="alert alert-success">
                                {{ session()->get('success_message') }}
                            </div>
                        @endif
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div style="margin-top:0 !important;margin-bottom:16px;" class="section-title">
                            <h3 class="title" style="text-transform: uppercase;font-size:16px;">Account Information</h3>
                        </div>
                        <div style="margin-top:30px;">
                                <p style="font-size:12px;font-weight:600">Email</p>
                                <div class="form-group">
                                 <input class="input" type="text" id="email" name="email" disabled value={{Auth::user()->email}}>
                                </div>
                        </div>
                        <div style="margin-top:30px;">
                            <p style="font-size:12px;font-weight:600">Name<span style="color:red;"> *</span></p>
                            <div class="form-group">
                                <input class="input" type="text" id="name" name="name" required value="{{Auth::user()->name}}">
                            </div>
                        </div>
                        <div class="input-checkbox">
                            <input type="checkbox" id="change-password" onclick="showChangePasswordForm()">
                            <label for="change-password">
                                <span></span>
                                Change Password
                            </label>
                            <div class="caption" style="margin-top:20px;">
                                <div style="margin-bottom:16px;" class="section-title">
                                        <h3 class="title" style="text-transform: uppercase;font-size:16px;">Change Password</h3>
                                </div>
                                <div style="margin-top:30px;">
                                    <p style="font-size:12px;font-weight:600">New Password<span style="color:red;"> *</span></p>
                                    <div class="form-group">
                                        <input class="input hidefield" type="text" id="newpassword" name="newpassword">
                                    </div>
                                </div>
                                <div style="margin-top:30px;">
                                    <p style="font-size:12px;font-weight:600">Confirm New Password<span style="color:red;"> *</span></p>
                                    <div class="form-group">
                                        <input class="input hidefield" type="text" id="password_confirmation"  name="password_confirmation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="primary-btn cta-btn" title="Save"><span>Save</span></button>
                    </div>
                </form>
                <!-- /STORE -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->
@endsection