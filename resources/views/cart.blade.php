@extends('layout')
@section('title','Shopping Cart')
@section('content')
	<div id="breadcrumb" class="section">
		<!-- container -->
		<div class="container">
				<!-- row -->
				<div class="row">
						<div class="col-md-12">
								<ul class="breadcrumb-tree">
										<li><a href="/">Home</a></li>
										<li class="active">Shopping Cart</li>
								</ul>
						</div>
				</div>
				<!-- /row -->
		</div>
		<!-- /container -->
	</div>
			
	<div class="section">
		<div class="container">
			<div class="row">
				<!-- section title -->
				<div class="col-md-8" style="padding-bottom:5%;">
					@if(Session::has('message'))
						<div style="margin-top:2%;" class="alert alert-{{ session('messageLevel') }}" id="MessageBox">
							{{Session::get('message')}}
						</div>
					@endif
					<div class="section-title">
						<h3 class="title">Shopping Cart</h3>
					</div>
					<div style="margin-bottom: 2%;">
						<ul class="breadcrumb-tree">
							<li class="active">You currently have {{$cartProducts->count()}} item(s) in your cart.</li>
						</ul>
					</div>
					@if (count($cartProducts) > 0)
						<div>
							<div class="box">           
								<div class="table-responsive">
									<table class="table">
										<thead  style="background-color: #D10024;color:#FFF">
											<tr>
												<th colspan="2">Product</th>
												<th>Quantity</th>
												<th colspan="2">Total (RM)</th>
											</tr>
										</thead>
										<tbody>
											@foreach($cartProducts as $cartProduct)
												<tr>
													<td><a href="/shop/{{$cartProduct->slug}}"><img src="{{asset('img/productsImage/'.$cartProduct->slug.'.jpg')}}" alt="" height="50" weight="50">	</a></td>
													<td><a href="/shop/{{$cartProduct->slug}}">{{$cartProduct->name}}</a><div class="cart-table-description">{{$cartProduct->description}}</div></td>
													{{-- class="productquantity" --}}
													<td><input data-id={{$cartProduct->id}} type="number" min="1" class="productquantity" oninput="validity.valid||(value='1');" value="{{$cartProduct->product_quantity}}" required></td>
												
													<td>RM  {{$cartProduct->total_product_price}}</td>
													<td><a href="/cart/delete/{{$cartProduct->id}}"><i class="fa fa-trash-o"></i></a></td>
												</tr>
											@endforeach
											
										</tbody>
										<tfoot>
											<tr>
												<th colspan="3">Total</th>
												<th colspan="2">RM {{$totalprice}}</th>
											</tr>
										</tfoot>
									</table>
								</div>
								<!-- /.table-responsive-->
								<div style="margin-bottom: 2%;" class="box-footer d-flex justify-content-between flex-column flex-lg-row">
									<div class="left">
										<a class="primary-btn cta-btn" href="#"><i class="fa fa-chevron-left"></i> Continue Shopping</a>
										<a style="float:right;" class="primary-btn cta-btn" href="/checkout"><i class="fa fa-chevron-right"></i> Proceed to Checkout</a>

										{{-- <input id="testid" type="text" class="form-control customInput" name="testid" placeholder="testid" >
										<a style="float:right;" class="primary-btn cta-btn" id="testbutton" href="/cart/update"><i class="fa fa-chevron-right"></i> Test</a> --}}
									</div>
								</div>
							</div>
						</div>
					@else
						<a class="btn btn-default" style="padding:15px;margin-top:2%;background-color:#D10024;color:white;" href="/">Continue Shopping</a>    
					@endif
				</div>
				
				@if (count($cartProducts) > 0)
					<div class="col-md-4" style="border:1px solid #e6e6e6;">
						<div class="section-title">
							<h3 class="title">Order Summary</h3>
						</div>
						<div>
							<div class="table-responsive">
								<table class="table">
									<tbody>
										<tr>
											<td>Order subtotal</td>
											<th>RM {{$totalprice}}</th>
										</tr>
										<tr>
											<td>Shipping and handling</td>
											<th>FREE</th>
										</tr>
										<tr class="total" style="font-weight: bold;">
											<td>Total INCL.TAX (0%)</td>
											<th>RM {{$totalprice}}</th>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>		
	{{-- @include('partials.relatedproduct') --}}
@endsection