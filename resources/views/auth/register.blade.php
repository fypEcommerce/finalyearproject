@extends('layout')
@section('title','Register')
@section('content')
<div class="container">
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7" style="border-right:1px solid #CDCDCD;">
                    <div style="text-transform: uppercase;font-weight: bold; padding:10px;border-bottom:1px solid #D10024;">
								Create an account
					</div>
                    @if(count($errors)>0)
                        <div style="margin-top:2%;" class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                    <form class="form-group" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div style="margin-top:5%;">
                            <input id="name" type="text" class="form-control customInput" name="name" placeholder="Enter Name" required autofocus>
                        </div>
                        <div style="margin-top:5%;">
                            <input id="email" type="email" name="email" class="form-control customInput" placeholder="Enter Email" required>
                        </div>
                        <div style="margin-top:5%;">
                            <input id="confirmPassword" type="password" class="customInput form-control" minlength="6" name="password" placeholder="Password" required>
                        </div>
                        <div style="margin-top:5%;">
                            <input id="password-confirm" type="password" name="password_confirmation" class="form-control customInput" placeholder="Re-enter Password" required>
                        </div>
                        <div style="width:100%;">
                            <div style="float:left;width:50%;">
                                <button class="createAccountBtn" type="submit">
                                    Create an account
                                </button>
                            </div>
                            <div style="float:right;text-align:right;margin-top:2%;width:50%;">
                                <p><strong>Already have an account?</strong></p>
                                <a href="/login">Login</a>
                            </div>           
						</div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <div>
                        <div style="text-transform: uppercase;font-weight: bold; padding:10px;border-bottom:1px solid #D10024;margin-bottom:12px;">
                            New Customer
                        </div>
                        <p><strong>Save time now.</strong></p>
                        <p>Creating an account will allow you to checkout faster in the future, have easy access to order history and customize your experience to suit your preferences.</p>

                        <p><strong>Loyalty Program</strong></p>
                        <p>Create an new account to enjoy special offers !</p>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
