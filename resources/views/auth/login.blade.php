@extends('layout')
@section('title','Login')
@section('content')
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7" style="border-right:1px solid #CDCDCD;">
                <div style="text-transform: uppercase;font-weight: bold; padding:10px;border-bottom:1px solid #D10024;">
                        Existing Customer
                </div>
                @if(count($errors) > 0)
                <div style="margin-top:2%;" class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form method="post" class="form-group" action="{{ route('login') }}">
                    @csrf
                    <div style="margin-top:2%;">
                        <input type="email" class="form-control customInput" name="email" placeholder="Email" required="" minlength="3" aria-required="true">
                    </div>
                    <div style="margin-top:5%;">
                        <input type="password" class="form-control customInput" name="password" placeholder="Password" required="" minlength="3" aria-required="true">
                    </div>
                    <button class="createAccountBtn" type="submit">
                        Login
                    </button>
                </form>
            </div>
            <div class="col-xs-12 col-sm-5">
                <div class="auth-right">
                    <div style="text-transform: uppercase;font-weight: bold; padding:10px;border-bottom:1px solid #D10024;margin-bottom:12px;">
                        New Customer
                    </div>
                    <p><strong>Still dont have an account?</strong></p>
                    <p>Sign up now to enjoy special offers !</p>
                    <a href="/register"><button class="createAccountBtn2">
                            Create Account
                    </button></a>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection
