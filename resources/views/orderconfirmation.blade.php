@extends('layout')
@section('title','Thank you')
@section('content')

    <div class="section">
        <div class="container">
            <div class=row>
                <div style="text-align:center;height:500px;padding-top:200px;" class="col-md-12">
                    <h1 style="color:black;"><strong>Thank you for Your Order!</strong></h1>
                    <p><h5>Your order will be processed accordingly</h5></p>
                    <div>
                        <a class="primary-btn cta-btn" href="/"> Home Page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
@endsection