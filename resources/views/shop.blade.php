@extends('layout')
@section('title','Reseult For "Product Name"')	
@section('content')
	<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumb-tree">
							<li><a href="/">Home</a></li>
							<li class="active">Shop</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
	</div>

	<div class="section">
		<div class="container">
			<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- ASIDE -->
			
				<!-- STORE -->
				<div id="store" class="col-md-30">
					<!-- store products -->
					<div class="row">
						
						@foreach($products as $product)
							<a href="/shop/{{$product->slug}}">
								<div class="col-md-4 col-xs-6">
									<div class="product">
										<div class="product-img">
											<img src="{{asset('img/productsImage/'.$product->slug.'.jpg')}}" alt="">	
										</div>
										<div class="product-body">
											<h3 class="product-name"><a href="/shop/{{$product->slug}}">{{$product->name}}</a></h3>
											<h4 class="product-price">RM {{$product->price_per_product}}</h4>
										</div>
									</div>
								</div>
							</a>
						@endforeach
						
						<!-- /product -->
					</div>
					<!-- /store products -->

					<!-- store bottom filter -->
					<div class="store-filter clearfix">
						<span class="store-qty">Showing 9-239 products</span>
						<ul class="store-pagination">
							<li class="active">1</li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div>
					<!-- /store bottom filter -->
				</div>
				<!-- /STORE -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /SECTION -->
		</div>
	<div>
@endsection