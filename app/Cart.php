<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';
    protected $fillable = [
        'customer_id',
    ];

    public function getCartByCustomerID($customer_id){
        $cart = DB::table('carts')
                ->WHERE('customer_id',$customer_id)
                ->first();
        return $cart;
    }

}
