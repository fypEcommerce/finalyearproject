<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Shipping_Address extends Model
{
    protected $table = 'shipping_addresses';

    protected $fillable = [
        'name','phone','address','state','city','postcode','defaultshipping','customer_id',
    ];

    public function getShippingAddressByCustomerID($customer_id){
        $shipping_addresses = DB::table('shipping_addresses')
                ->WHERE('customer_id',$customer_id)
                ->WHERE('deleteflag',"0")
                ->get();
        return $shipping_addresses;
    }

    public function getShippingAddressByAddressID($address_id){
        $shipping_address = DB::table('shipping_addresses')
                ->WHERE('id',$address_id)
                ->first();
        return $shipping_address;
    }

    public function getCustomerDefaultAddress($customer_id){
        $shipping_address = DB::table('shipping_addresses')
                ->WHERE('customer_id',$customer_id)
                ->WHERE('defaultshipping',"T")
                ->first();
        return $shipping_address;
    }

}
