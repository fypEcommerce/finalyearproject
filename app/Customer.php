<?php

namespace App;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $table = 'customers';
    protected $fillable = [
        'name','email', 'password',
    ];

    public function getCustomerByEmail($email){
        $customer = DB::table('customers')
        ->WHERE('email',$email)
        ->first();
        return $customer;
    }
}
