<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = "coupons";

    public function getCoupon($coupon_code){
        $coupon = DB::table('coupons')
                ->WHERE('coupon_code',$coupon_code)
                ->first();
        return $coupon;
    }
}
