<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $appends = ['order_products'];
    protected $fillable = [
        'total_amount', 'total_quantity','payment_gateway','status','customer_id','shipping_address_id'
    ];



    public function getOrdersByCustomerID($customer_id){
        $orders = DB::table('orders')
                ->WHERE('customer_id',$customer_id)
                ->get();
        return $orders;
    }

    public function getOrdersByOrderID($order_id){
        $orders = DB::table('orders')
                ->WHERE('id',$order_id)
                ->first();
        return $orders;
    }

    public function getTestingAttribute()
    {
        return $this->attributes['admin'] == 'yes';
    }
}
