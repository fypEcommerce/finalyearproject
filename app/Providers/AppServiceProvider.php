<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use App\Cart;
use App\Cart_Product;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
      
        view()->composer('*', function ($view) 
        {
            if (Auth::check()) {
                $cart = new Cart();
                $cart = $cart->getCartByCustomerID(Auth::user()->id);

                $cartProducts = new Cart_Product();
                $cartProducts = $cartProducts->getCartProducts($cart->id);

                $view->with('cart_count', $cartProducts->count());    
            }
        }); 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
