<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Cart_Product extends Model
{
    protected $table ="cart_products";
    protected $fillable = [
        'cart_id', 'product_id','product_quantity','total_product_price',
    ];

    public function getCartProducts($cart_id){
        $cartproduct = DB::table('cart_products')
                    ->JOIN('products','cart_products.product_id','=','products.id')
                    ->WHERE('cart_id',$cart_id)
                    ->get();
        return $cartproduct;
    }

    public function checkProductExist($cart_id,$product_id){
        $ProductExist = DB::table('cart_products')
                ->WHERE('cart_id',$cart_id)
                ->WHERE('product_id',$product_id)
                ->count();
        return $ProductExist;
    }
    
}
