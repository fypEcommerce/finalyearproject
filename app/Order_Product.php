<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order_Product extends Model
{
    protected $table = 'order_products';

    protected $fillable = [
        'order_id', 'product_id', 'price','quantity',
    ];
    
    public function getOrderProductByOrderID($order_id){
        $order_products = DB::table('order_products')
                ->JOIN('products','order_products.product_id','=','products.id')
                ->WHERE('order_id',$order_id)
                ->get();
        return $order_products;
    }


}
