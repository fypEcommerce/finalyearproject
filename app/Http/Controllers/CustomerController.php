<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\Order_Product;
use App\Shipping_Address;
use DB;
class CustomerController extends Controller
{
    public function showProfile(){
        return view('profile');
    }

    public function updateAccount(Request $request){
        $request->validate([
            'name' =>'required|string|max:255',
            'newpassword' =>'sometimes|nullable|string|min:3',
            'password_confirmation' =>'sometimes|same:newpassword',
        ]);

        $user = Auth()->user();
        $input =$request->except('newpassword','password_confirmation');

        if (!$request->filled('newpassword')){
            $user->fill($input)->save();
            return back()->with('success_message','Profile updated successfully!');
        }
        
        $user->password = bcrypt($request->newpassword);
        $user->fill($input)->save();
        return back()->with('success_message','Profile updated successfully!');

    }

    public function viewOrderSummary(){
        $orders = new Order();
        $orders = $orders->getOrdersByCustomerID(Auth::user()->id);

        foreach($orders as $order){
            $order_products = new Order_Product();
            $order_products =  $order_products->getOrderProductByOrderID($order->id);
            $order->order_products = $order_products;
        }
        
        return view('order-summary')->with([
            'orders' =>$orders,
        ]);
    }

    public function viewOrderDetail($order_id){
        $order = new Order();
        $order = $order->getOrdersByOrderID($order_id);
        
        $order_products = new Order_Product();
        $order_products = $order_products->getOrderProductByOrderID($order_id);
        
        $shipping_address = new Shipping_Address();
        $shipping_address = $shipping_address->getShippingAddressByAddressID($order->shipping_address_id);

        return view('order-detail')->with([
            'order' => $order,
            'order_products' =>$order_products,
            'shipping_address' => $shipping_address,
        ]);
    }

    public function showAddressBook(){
        $shipping_addresses = new Shipping_Address();
        $shipping_addresses = $shipping_addresses->getShippingAddressByCustomerID(Auth::user()->id);
        
        return view('addressbook')->with([
            'shipping_addresses' =>$shipping_addresses,
        ]);
    }

    public function saveNewAddress(Request $request){
        if ($request->has('default')){
            //Remove default from old shipping address when new shipping address set as default shipping
            $shipping_address = DB::table('shipping_addresses')
                        ->where('customer_id', Auth::user()->id)
                        ->update(['defaultshipping' => "F"]);       


            $new_shipping_address = Shipping_Address::create([
                'name' => $request->name,
                'phone' => $request->phonenumber ,
                'address' => $request->address,
                'state' => $request->state,
                'city' => $request->city,
                'postcode' => $request->postalcode,
                'defaultshipping' => $request->default,
                'customer_id' => Auth::user()->id,
            ]);
        }
        else{
            $new_shipping_address = Shipping_Address::create([
                'name' => $request->name,
                'phone' => $request->phonenumber ,
                'address' => $request->address,
                'state' => $request->state,
                'city' => $request->city,
                'postcode' => $request->postalcode,
                'customer_id' => Auth::user()->id,
            ]);
        }
        return back();
    }

    public function editAddress(Request $request){
        $shipping_address = DB::table('shipping_addresses')
                            ->where('id', $request->addressid)
                            ->update(['name' => $request->name,'phone' => $request->phonenumber,'address' => $request->address,'state' => $request->state,'city' => $request->city,'postcode' => $request->postalcode]);
        return back();
    }

    public function setDefaultAddress($address_id){
        
        $shipping_address = DB::table('shipping_addresses')
                        ->where('customer_id', Auth::user()->id)
                        ->update(['defaultshipping' => "F"]);  

        $shipping_address = DB::table('shipping_addresses')
                        ->where('customer_id', Auth::user()->id)
                        ->where('id',$address_id)
                        ->update(['defaultshipping' => "T"]);  

        return back();
    }

    public function deleteAddress($address_id){
        
        $shipping_address = new Shipping_Address();
        $shipping_address = $shipping_address->getShippingAddressByAddressID($address_id);
        
        if ($shipping_address->defaultshipping != "T"){
            DB::table('shipping_addresses')
                ->where('id',$address_id)
                ->update(['deleteflag' => "1"]); 
            return back();
        }else{
            return back()->withErrors('Cannot delete default address.');
        }        

    }

    public function ajaxRetrieveAddress(Request $request){
        $shipping_address = new Shipping_Address();
        $shipping_address = $shipping_address->getShippingAddressByAddressID($request->addressid);
        
        $output = array(
            'addressid' => $request->addressid,
            'name' => $shipping_address->name,
            'phone' => $shipping_address->phone,
            'address' => $shipping_address->address,
            'state' => $shipping_address->state,
            'city' => $shipping_address->city,
            'postcode' => $shipping_address->postcode
        );

        echo json_encode($output);
      

    }
}
