<?php


namespace App\Http\Controllers;
use Auth;
use View;
use App\Product;
use App\Cart;
use App\Cart_Product;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::inRandomOrder()->take(9)->get();
        return view('landing-page')->with('products',$products);
    }

  
}
