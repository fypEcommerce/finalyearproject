<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderConfirmationPageController extends Controller
{
    public function showConfirmationPage(){
        if(!session()->has('success_message')){
            return redirect('/');
        }
        return view('orderconfirmation');
    }
}
