<?php

namespace App\Http\Controllers;
use App\Product;
use DB;
use Illuminate\Http\Request;

class ShopPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::inRandomOrder()->take(8)->get();
        return view('shop')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function showProduct($slug)
    {
        
        $product = new Product();
        $product = $product->getProduct($slug);
        
        $relatedProducts = DB::select("SELECT * FROM products WHERE slug!='$slug' ORDER BY RAND() LIMIT 4");
         
        return view('product')->with([
            'product'=> $product,
            'relatedProducts'=>$relatedProducts,
        ]);
    }

 
}
