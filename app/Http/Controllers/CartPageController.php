<?php

namespace App\Http\Controllers;
use App\Product;
use App\Cart;
use App\Cart_Product;
use App\Customer;
use Auth;
use DB;
use Illuminate\Http\Request;

class CartPageController extends Controller
{
    public function index(){
        // $relatedProducts = DB::select("SELECT * FROM products ORDER BY RAND() LIMIT 4");
        // return view('cart')->with('relatedProducts',$relatedProducts);
        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);

        $cartProducts = new Cart_Product();
        $cartProducts = $cartProducts->getCartProducts($cart->id);

        $totalprice = 0;
        foreach($cartProducts as $cartProduct){
            $totalprice = $totalprice + ($cartProduct->price_per_product*$cartProduct->product_quantity);
        }

        return view('cart')->with([
            'cartProducts'=> $cartProducts,
            'totalprice' => $totalprice,
        ]);

    }

    public function addItem(Request $request){
       
        // $request->ProductID = 4;
        // $request->NewQty = 5;
       
        // $product = new Product();
        // $product = $product->getProductByID($request->ProductID);
      
        // $cart = new Cart();
        // $cart = $cart->getCartByCustomerID(Auth::user()->id);
      
        // $cart_product = DB::table('cart_products')
        //                 ->where('cart_id', $cart->id)
        //                 ->where('product_id', $product->id)
        //                 ->update(['product_quantity' => $request->NewQty,'total_product_price'=>$request->NewQty * 100]);
       

        // dd($request->all()); productID productquantity
        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);
        
        $checkProductExistInCart = new Cart_Product();
        $checkProductExistInCart = $checkProductExistInCart->checkProductExist($cart->id,$request->productID);

        if($checkProductExistInCart == 0){
            $product = new Product();
            $product = $product->getProductByID($request->productID);

            $total_product_price = ($request->productquantity)*($product->price_per_product);
            $cart_product = Cart_Product::create([
                'cart_id' => $cart->id,
                'product_id' => $product->id,
                'product_quantity' => $request->productquantity,
                'total_product_price' => $total_product_price,
            ]);
            $message = "Product Added Successfully";
            $messageLevel = "success";
        }else{
            $message = "Product Already in Shopping Cart";
            $messageLevel = "danger";
        }

        return redirect('/cart')->with([
            'message' => $message,
            'messageLevel' => $messageLevel,
        ]);
    }

    public function deleteItem($productID){
        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);

        DB::table('cart_products')->WHERE('product_id',$productID)->WHERE('cart_id',$cart->id)->delete();

        return redirect('/cart')->with([
            'success_message' => 'Product has been successfully removed from Shopping Cart ',
        ]);

    }

    public function updateQuantity(Request $request){

        // $request->ProductID = 4;
        // $request->NewQty = $request->name;
       
        $product = new Product();
        $product = $product->getProductByID($request->ProductID);
      
        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);
      
        $cart_product = DB::table('cart_products')
                        ->where('cart_id', $cart->id)
                        ->where('product_id', $product->id)
                        ->update(['product_quantity' => $request->NewQty,'total_product_price'=>$request->NewQty * $product->price_per_product]);       
    }
}

