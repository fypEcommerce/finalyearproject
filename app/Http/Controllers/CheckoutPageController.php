<?php

namespace App\Http\Controllers;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Cartalyst\Stripe\Exception\CardErrorException;
use Illuminate\Http\Request;
use Auth;
use App\Cart;
use App\Cart_Product;
use App\Order;
use App\Order_Product;
use App\Coupon;
use App\Shipping_Address;
use DB;

class CheckoutPageController extends Controller
{
    public function getCheckout(){
        $default_shipping = new Shipping_Address();
        $default_shipping = $default_shipping->getCustomerDefaultAddress(Auth::user()->id);
     
        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);

        $cartProducts = new Cart_Product();
        $cartProducts = $cartProducts->getCartProducts($cart->id);
        
        $totalprice = 0;
        foreach($cartProducts as $cartProduct){
            $totalprice = $totalprice + ($cartProduct->price_per_product*$cartProduct->product_quantity);
        }

        if(session()->has('coupon')){
            $discount = session()->get('coupon')['discount'];
        }else{
            $discount = 0;
        }
        
        $Newtotalprice = $totalprice - $discount;
     
        
        $gateway = new \Braintree\Gateway([
            'environment' => config('services.braintree.environment'),
            'merchantId' => config('services.braintree.merchantId'),
            'publicKey' => config('services.braintree.publicKey'),
            'privateKey' => config('services.braintree.privateKey')
        ]);
     
        $paypalToken = $gateway->clientToken()->generate();

        return view('/checkout')->with([
            'paypalToken' => $paypalToken,
            'cartProducts'=> $cartProducts,
            'totalPrice' => $totalprice,
            'Newtotalprice' => $Newtotalprice,
            'defaultShipping' => $default_shipping,
        ]);
    }


    public function stripeCheckout(Request $request){

        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);

        $cartProducts = new Cart_Product();
        $cartProducts = $cartProducts->getCartProducts($cart->id);
    
        $contents = $cartProducts->map(function($cartProducts){
            return 'Product=>'.$cartProducts->name.',Quantity=>'.$cartProducts->product_quantity;
        })->values()->toJson();

        try{
            $charge = Stripe::charges()->create([
                'amount' => $request->Newtotalprice,
                'currency' => 'MYR',
                'source' => $request->stripeToken,
                'description' => 'Order',
                'receipt_email' => $request->email,
                'metadata' => [
                    'contents' => $contents,
                    'discount' => collect(session()->get('coupon'))->toJson(),
                ],
            ]);
        
            //Create Order
            $this->createOrder($request,$cartProducts,'Stripe','succeeded');

 
            //Clear Shopping Cart Item After Success Checkout
            DB::table('cart_products')->WHERE('cart_id',$cart->id)->delete();

            //Clear coupon if coupon applied
            session()->forget('coupon');

            return redirect('/thankyou')->with('success_message','Thank you ! Your payment has been successfully accepted!');
       
        }catch(CardErrorException $e){
            //Create Order
            $this->createOrder($request,$cartProducts,'Stripe',$e->getMessage());
            return back()->withErrors('Error ! ' . $e->getMessage());
        }
    }

    public function paypalCheckout(Request $request){
        $cart = new Cart();
        $cart = $cart->getCartByCustomerID(Auth::user()->id);

        $cartProducts = new Cart_Product();
        $cartProducts = $cartProducts->getCartProducts($cart->id);


        $gateway = new \Braintree\Gateway([
            'environment' => config('services.braintree.environment'),
            'merchantId' => config('services.braintree.merchantId'),
            'publicKey' => config('services.braintree.publicKey'),
            'privateKey' => config('services.braintree.privateKey')
        ]);

        $nonce = $request->payment_method_nonce;
      
    
        $result = $gateway->transaction()->sale([
            'amount' => $request->Newtotalprice,
            'paymentMethodNonce' => $nonce,
            // 'customer' => [
            //     'firstName' => 'Tony',
            //     'lastName' => 'Stark',
            //     'email' => 'tony@avengers.com',
            // ],
            'options' => [
                'submitForSettlement' => true
            ]
        ]);
    
        if ($result->success) {
            $transaction = $result->transaction;
           //dd($transaction->paypal);

            //Create Order
            $this->createOrder($request,$cartProducts,'Paypal','succeeded');

            //Clear Shopping Cart Item After Success Checkout
            DB::table('cart_products')->WHERE('cart_id',$cart->id)->delete();

            //Clear coupon if coupon applied
            session()->forget('coupon');

            return redirect('/thankyou')->with('success_message','Thank you ! Your payment has been successfully accepted!');
        } else {
            //Create Order
            $this->createOrder($request,$cartProducts,'Paypal',$result->message);

            return back()->withErrors('An error occurred with the message: '.$result->message);
        }
    }


    public function createOrder($request,$cartProducts,$paymentGateway,$error){
        $customer_shipping_address = new Shipping_Address();
        $customer_shipping_address = $customer_shipping_address->getCustomerDefaultAddress(Auth::user()->id);

        $total_quantity = 0;
        foreach($cartProducts as $cartProduct){
            $total_quantity = $total_quantity + $cartProduct->product_quantity;
        }
        $order = Order::create([
            'total_amount' => $request->Newtotalprice,
            'total_quantity' =>$total_quantity ,
            'payment_gateway' =>$paymentGateway,
            'status' => $error,
            'customer_id' => Auth::user()->id,
            'shipping_address_id' => $customer_shipping_address->id,
        ]);
        
        if($error == "succeeded"){
            foreach($cartProducts as $cartProduct){
                $order_product = Order_Product::create([
                    'order_id' => $order->id,
                    'product_id' =>$cartProduct->product_id ,
                    'price' => $cartProduct->total_product_price,
                    'quantity' => $cartProduct->product_quantity,
                ]);
            }
        }
    }

    public function checkCoupon(Request $request){
        $coupon = new Coupon();
        $coupon = $coupon->getCoupon($request->coupon_code);
        
        if (!$coupon){
            return redirect('/checkout')->withErrors('Invalid coupon code. Please try again.');
        }

        session()->put('coupon',[
            'name' => $coupon->coupon_code,
            'discount' => $coupon->discount_price, 
        ]) ;

        
        return redirect('/checkout')->with('success_message', 'Coupon has been applied!');
    }

    public function removeCoupon(){
        session()->forget('coupon');
        return back()->with('success_message','Coupon has been removed!');
    }
}