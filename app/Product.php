<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    
    public function getProduct($slug){
        $product = DB::table('products')
                    ->WHERE('slug',$slug)
                    ->first();
        return $product;
    }

    public function getProductByID($product_id){
        $product = DB::table('products')
                    ->WHERE('id',$product_id)
                    ->first();
        return $product;
    }
}
